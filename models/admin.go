package models

// Admin модель админа в MongoDB
type Admin struct {
	Username string `json:"login"`
	Password string `json:"password"`
}
