package models

import "gopkg.in/mgo.v2/bson"

// Item model
type Item struct {
	Id          bson.ObjectId `bson:"_id" json:"id"`
	Name        string        `json:"name" binding:"required"`
	Cloth       string        `json:"cloth" binding:"required"`
	Family      bool          `json:"family" binding:"required"`
	Woman       bool          `json:"woman" binding:"required"`
	Child       bool          `json:"child" binding:"required"`
	Description string        `json:"description" binding:"required"`
	Price       float32       `json:"price" binding:"required"`
	Photo       string        `json:"photo" binding:"required"`
}

type ItemWithoutId struct {
	Name        string  `json:"name"`
	Cloth       string  `json:"cloth"`
	Family      bool    `json:"family"`
	Woman       bool    `json:"woman"`
	Child       bool    `json:"child"`
	Description string  `json:"description"`
	Price       float32 `json:"price"`
	Photo       string  `json:"photo"`
}
