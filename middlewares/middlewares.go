package middlewares

import (
	"net/http"

	"github.com/antonkri97/Slivky/db"
	"github.com/antonkri97/Slivky/utils"
	"gopkg.in/gin-gonic/gin.v1"
)

// AuthRequired проверяет валиднодсть токена
func AuthRequired(c *gin.Context) {
	h := c.Request.Header.Get("Authorization")

	if n := len(h); n == 0 || n > 130 {
		c.AbortWithStatus(http.StatusUnauthorized)
		return
	}
	if !utils.ValidateToken(h) {
		c.AbortWithStatus(http.StatusUnauthorized)
	}
	c.Next()
}

// Connect middleware clones the database session for each request and
// makes the `db` object available for each handler
func Connect(c *gin.Context) {
	s := db.Session.Clone()
	defer s.Close()

	c.Set("db", s.DB(db.Mongo.Database))
	c.Next()
}

// ErrorHandler is a middleware to handle errors encountered during requests
func ErrorHandler(c *gin.Context) {
	c.Next()

	if len(c.Errors) > 0 {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors": c.Errors,
		})
	}
}

// CORSMiddleware allowing CORS
func CORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
		c.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	}
}
