package utils

import (
	"fmt"
	"log"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
)

// GenerateToken generate valid token
func GenerateToken() string {
	expireToken := time.Now().Add(time.Hour * 1).Unix()

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"exp": expireToken,
	})

	signedToken, _ := token.SignedString([]byte("e9e{NUXzgP=j8)*[mJ+K+NB7M7"))

	return signedToken
}

// ValidateToken проверяет токен
func ValidateToken(tokenString string) bool {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return []byte("e9e{NUXzgP=j8)*[mJ+K+NB7M7"), nil
	})

	if _, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		return true
	}
	log.Println(err)
	return false
}
