package handlers

import (
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/gin-gonic/gin"
)

func TestGetNumbers(t *testing.T) {
	router := gin.New()
	router.GET("/hello", Test)

	req, err := http.NewRequest("GET", "/hello", nil)
	if err != nil {
		t.Fatalf("could not create request %v", err)
	}

	res := httptest.NewRecorder()

	router.ServeHTTP(res, req)

	if res.Code != http.StatusOK {
		t.Errorf("expected status 200: got %d", res.Code)
	}

	if strings.Contains(res.Body.String(), `{"message":"Hi!"}\n`) {
		t.Errorf("unexpected body in response: %q", res.Body.String())
	}
}
