package handlers

import (
	"net/http"

	"github.com/antonkri97/Slivky/models"
	"gopkg.in/gin-gonic/gin.v1"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"golang.org/x/crypto/bcrypt"
)

// Hello to user
func Hello(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"message": "Hi!",
	})
}

// AllItems get all items from db
func AllItems(c *gin.Context) {
	db := c.MustGet("db").(*mgo.Database)

	items := []models.Item{}

	err := db.C("items").Find(nil).All(&items)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"msg": "can't get all items"})
		return
	}

	c.JSON(http.StatusOK, items)
}

func ItemById(c *gin.Context) {
	db := c.MustGet("db").(*mgo.Database)

	id := c.Param("id")

	item := models.Item{}

	err := db.C("items").FindId(bson.ObjectIdHex(id)).One(&item)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"msg": "can't get item"})
		return
	}

	c.JSON(http.StatusOK, item)
}

func CreateItem(c *gin.Context) {
	db := c.MustGet("db").(*mgo.Database)

	item := models.ItemWithoutId{}

	if c.BindJSON(&item) != nil {
		c.Status(http.StatusBadRequest)
		return
	}

	err := db.C("items").Insert(item)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"msg": "can't insert item"})
		return
	}

	c.Status(http.StatusCreated)
}

func DeleteItem(c *gin.Context) {
	db := c.MustGet("db").(*mgo.Database)

	id := c.Param("id")

	err := db.C("items").RemoveId(bson.ObjectIdHex(id))
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"msg": "can't delete item"})
		return
	}

	c.Status(http.StatusOK)
}

func UpdateItem(c *gin.Context) {
	db := c.MustGet("db").(*mgo.Database)

	id := c.Param("id")

	item := models.ItemWithoutId{}

	if c.BindJSON(&item) != nil {
		c.Status(http.StatusBadRequest)
		return
	}

	err := db.C("items").UpdateId(bson.ObjectIdHex(id), item)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"msg": "can't update item"})
		return
	}

	c.Status(http.StatusOK)
}

func GeneratePassword(c *gin.Context) {
	db := c.MustGet("db").(*mgo.Database)

	pass, err := bcrypt.GenerateFromPassword([]byte("password"), bcrypt.DefaultCost)
	if err != nil {
	        c.JSON(http.StatusInternalServerError, gin.H{"msg": "can't generate password"})
		return
	}

	err = db.C("admin").Insert(bson.M{"username": "anton", "password": string(pass)})
	if err != nil {
	        c.JSON(http.StatusInternalServerError, gin.H{"msg": "can't insert document"})
		return
	}

	c.Status(http.StatusCreated)
}

func Ping(c *gin.Context) {
	c.String(http.StatusOK, "pong")
}
