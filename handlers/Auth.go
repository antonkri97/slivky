package handlers

import (
	"net/http"

	"github.com/antonkri97/Slivky/models"
	"github.com/antonkri97/Slivky/utils"
	"golang.org/x/crypto/bcrypt"
	"gopkg.in/gin-gonic/gin.v1"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// Login авторизация
func Login(c *gin.Context) {
	db := c.MustGet("db").(*mgo.Database)

	dbUser := models.Admin{}

	username := c.Query("username")
	password := c.Query("password")

	err := db.C("admin").Find(bson.M{"username": username}).One(&dbUser)
	if err != nil {
		c.Status(http.StatusUnauthorized)
		return
	}

	err = bcrypt.CompareHashAndPassword([]byte(dbUser.Password), []byte(password))
	if err != nil {
		c.Status(http.StatusUnauthorized)
		return
	}

	c.String(http.StatusOK, utils.GenerateToken())
}
