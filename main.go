package main

import (
	"gopkg.in/gin-gonic/gin.v1"
	"github.com/antonkri97/Slivky/db"
	"github.com/antonkri97/Slivky/middlewares"
	"github.com/antonkri97/Slivky/handlers"
	"github.com/antonkri97/Slivky/utils"
)

// Port is default port
const (
	Port = "8080"
)

func init() {
	db.Connect()
}

func main() {
	router := gin.Default()

	router.Use(middlewares.Connect)
	router.Use(middlewares.CORSMiddleware())

	authorized := router.Group("/")

	authorized.Use(middlewares.AuthRequired)
	{
		authorized.GET("/items", handlers.AllItems)
		authorized.GET("/items/:id", handlers.ItemById)
		authorized.POST("/items", handlers.CreateItem)
		authorized.PUT("/items/:id", handlers.UpdateItem)
		authorized.DELETE("/items/:id", handlers.DeleteItem)
	}

	router.POST("/login", handlers.Login)

	port := utils.CheckEnv("PORT", Port)

	router.Run(":" + port)
}
